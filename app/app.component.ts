import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'fc-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent { name = 'Angular'; }
